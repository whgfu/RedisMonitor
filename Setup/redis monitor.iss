; 脚本由 Inno Setup 脚本向导 生成！
; 有关创建 Inno Setup 脚本文件的详细资料请查阅帮助文档！

#define MyAppName "Redis 控制台"
#define MyAppVersion "1.5"
#define MyAppPublisher "rwecho"
#define MyAppExeName "RedisMonitor.exe"

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
; (生成新的GUID，点击 工具|在IDE中生成GUID。)
AppId={{0C80AA70-F07B-47A1-9798-DD625FFE31EE}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputBaseFilename=Redis 控制台
OutputDir=Output
Compression=lzma
SolidCompression=yes

[Languages]
Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce; 

[Files]
Source: "..\RedisMonitor\bin\Release\FirstFloor.ModernUI.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\ICSharpCode.AvalonEdit.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\install.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Expression.Interactions.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Practices.Prism.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Practices.Prism.Interactivity.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Practices.Prism.UnityExtensions.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Practices.ServiceLocation.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Practices.Unity.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Threading.Tasks.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Threading.Tasks.Extensions.Desktop.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Threading.Tasks.Extensions.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\Microsoft.Windows.Shell.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\RedisModule.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\RedisMonitor.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\RedisMonitor.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\RedisMonitor.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\StackExchange.Redis.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\System.IO.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\System.Net.Http.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\System.Runtime.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\System.Threading.Tasks.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\System.Windows.Interactivity.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\uninstall.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\RedisMonitor\bin\Release\config\*"; DestDir: "{app}\config"; Flags: ignoreversion          onlyifdoesntexist  uninsneveruninstall
         
; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion”

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"

[Run]
;Filename:"{app}\installservice.bat";Description:"注册服务";Flags:runhidden
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[UninstallRun]
;Filename:"{app}\unstallservice.bat";Flags:runhidden



[Code]
var 
  UserPage: TInputQueryWizardPage;
  DataDirPage: TInputDirWizardPage;

procedure AfterAppInstall();
var 
  nancyPort:String;
  rootFolder:String;
  FileName:String;
  content: String;
begin 
    nancyPort :=    UserPage.Values[0] ;
    rootFolder :=  DataDirPage.Values[0];
    FileName :=   ExpandConstant('{app}\config\app.config');
    try
       content :=  '<?xml version="1.0" encoding="utf-8"?><RESTFinder >    <NancyPort>' +nancyPort +'</NancyPort>   <RootPath>'+ rootFolder +'</RootPath> </RESTFinder>'
       SaveStringToFile(FileName, content, False);
    except
      MsgBox('An error occured during processing application ' +
        'config file!' + #13#10 + GetExceptionMessage, mbError, MB_OK);
    end;
end;

procedure CreateTheConfigPages;
var 
  Page: TWizardPage;
  
  Edit,Edit1: TNewEdit;
  StaticText, StaticText1: TNewStaticText;
begin
  { TButton and others }

  UserPage := CreateInputQueryPage(wpWelcome,
    '程序应用配置', '端口号',
    '请输入改程序开放的端口号,默认为8080.');
  UserPage.Add('NancyPort:', False);

  UserPage.Values[0] := '8080';

  DataDirPage := CreateInputDirPage(wpSelectDir,
    '选择文档存储主目录', '文档主目录?',
    '请选择最大的一个磁盘作为该程序的主目录,进行文档保存.',
    False, '');
  DataDirPage.Add('');

  DataDirPage.Values[0] := 'D:\Document Library';
 

end;

function GetUninstallString: string;
var
  sUnInstPath: string;
  sUnInstallString: String;
begin
  Result := '';
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{{0C80AA70-F07B-47A1-9798-DD625FFE31EE}_is1'); //Your App GUID/ID
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end;

function IsUpgrade: Boolean;
begin
  Result := (GetUninstallString() <> '');
end;

function InitializeSetup: Boolean;
var
  V: Integer;
  iResultCode: Integer;
  sUnInstallString: string;
begin
  Result := True; // in case when no previous version is found
  if RegValueExists(HKEY_LOCAL_MACHINE,'Software\Microsoft\Windows\CurrentVersion\Uninstall\{0C80AA70-F07B-47A1-9798-DD625FFE31EE}_is1', 'UninstallString') then  //Your App GUID/ID
  begin
    V := MsgBox(ExpandConstant('你好! 发现一个旧版本.是否要卸载它?'), mbInformation, MB_YESNO); //Custom Message if App installed
    if V = IDYES then
    begin
      sUnInstallString := GetUninstallString();
      sUnInstallString :=  RemoveQuotes(sUnInstallString);
      Exec(ExpandConstant(sUnInstallString), '', '', SW_SHOW, ewWaitUntilTerminated, iResultCode);
      Result := True; //if you want to proceed after uninstall
                //Exit; //if you want to quit after uninstall
    end
    else
      Result := True; //when older version present and not uninstalled
  end;

end;

procedure InitializeWizard();
var
  BackgroundBitmapImage: TBitmapImage;
  BackgroundBitmapText: TNewStaticText;
begin
  //CreateTheConfigPages;



end;


