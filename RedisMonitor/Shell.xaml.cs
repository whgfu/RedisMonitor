﻿using Microsoft.Windows.Shell;
using RedisDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RedisMonitor
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class Shell 
    {
        public Shell(ViewModels.ShellVm  vm)
        {
            InitializeComponent();
            this.Height = (System.Windows.SystemParameters.PrimaryScreenHeight * 0.75);
            this.Width = (System.Windows.SystemParameters.PrimaryScreenWidth * 0.75);

            DataContext = vm;

            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Help, OnCloseWindow));

        }

        private void OnCloseWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SettingsWindow child = new SettingsWindow();
            child.Owner = this;
            child.ShowDialog();
        }
    }
}
