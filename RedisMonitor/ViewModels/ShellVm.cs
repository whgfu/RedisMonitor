﻿using FirstFloor.ModernUI.Presentation;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Timers;
using System.Windows.Input;

namespace RedisMonitor.ViewModels
{
    public class ShellVm: INotifyPropertyChanged
    {
        private Timer timer = new Timer(1000);
        public ShellVm()
        {
            LeftRegion = "LeftRegion";
            CenterRegion = "CenterRegion";

            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
            timer.Start();

             RedisDomain.NotificationUtils.ShowAction = ShowMessage;
        }

       

        private void ShowMessage(string msg)
        {
            Notification = msg;

            RunStoryboard = true;
        }

        int i = 0;
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            RealTime = DateTime.Now;

            if(i>3)
            {
                i = 0;
                RunStoryboard = false;
            }
            if (RunStoryboard) i++;


            timer.Enabled = true;
        }

        public string LeftRegion { get; set; }
        public string CenterRegion { get; set; }

        private DateTime _realTime;

        public DateTime RealTime
        {
            get { return _realTime; }
            set { SetProperty(ref _realTime, value, () => RealTime); }
        }

        private string _notification;

        public string Notification
        {
            get { return _notification; }
            set { SetProperty(ref _notification, value, () => Notification); }
        }

        private bool _runStoryboard;

        public bool RunStoryboard
        {
            get { return _runStoryboard; }
            set { SetProperty(ref _runStoryboard, value, () => RunStoryboard); }
        }


        protected virtual bool SetProperty<T>(ref T storage, T value, Expression<Func<T>> propertyExpression)
        {
            if (object.Equals(storage, value)) return false;

            storage = value;
            this.OnPropertyChanged(propertyExpression);

            return true;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var propertyName = PropertySupport.ExtractPropertyName(propertyExpression);
            this.OnPropertyChanged(propertyName);
        }
    }
}
