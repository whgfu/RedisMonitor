﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using RedisDomain;

namespace RedisModule
{
    public class RedisModule : IModule
    {
        private IRegionManager _regionManager;

        public RedisModule(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
        }
        public void Initialize()
        {
            _regionManager.RegisterViewWithRegion("LeftRegion", typeof(Views.ConnectManageView));
            _regionManager.RegisterViewWithRegion("BottomRegion", typeof(Views.LoggerView));
        }
    }
}
