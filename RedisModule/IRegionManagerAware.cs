﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace RedisModule
{
    public interface IRegionManagerAware
    {
        IRegionManager RegionManager { get; set; }
    }
}
