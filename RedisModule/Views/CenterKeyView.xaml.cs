﻿using RedisModule.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RedisModule.Views
{
    /// <summary>
    /// CenterKeyView.xaml 的交互逻辑
    /// </summary>
    public partial class CenterKeyView : UserControl
    {
        public CenterKeyView(ConnectManageVm vm)
        {
            InitializeComponent();

            DataContext = vm.CurrentRedisConnection.CurrentRedisDatabase.CurrentRedisKey;
            //TxbHighLight.Text = @"{  'array': [    1,    2,    3  ],  'boolean': true,  'null': null,  'number': 123,  'object': {    'a': 'b',    'c': 'd',    'e': 'f'  }, 'string': 'Hello World'}";
        }
    }
}
