﻿using Microsoft.Practices.Prism.Logging;
using RedisDomain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RedisModule.Views
{
    /// <summary>
    /// LoggerView.xaml 的交互逻辑
    /// </summary>
    public partial class LoggerView : UserControl
    {
        public LoggerView()
        {
            InitializeComponent();
            Logger.LogCallback = Log;
        }

        public void Log(string message, Category category, Priority priority)
        {
            this.TraceTextBox.AppendText(string.Format(CultureInfo.CurrentUICulture, "[{0}][{1}] {2:yyyy-MM-dd HH:mm:ss} : {3} \r\n", category, priority, DateTime.Now,message));
        }
    }
}
