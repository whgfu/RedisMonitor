﻿using RedisModule.ViewModels;
using System.Windows.Controls;

namespace RedisModule.Views
{
    /// <summary>
    /// CenterView.xaml 的交互逻辑
    /// </summary>
    public partial class CenterConnectView : UserControl
    {
        public CenterConnectView(ConnectManageVm vm)
        {
            InitializeComponent();

            foreach (var item in vm.RedisConnectionVms)
            {
                if (item.ToString() == vm.CurrentRedisConnection.ToString())
                {
                    DataContext = item; break;
                }
            } 
        }
    }
}
