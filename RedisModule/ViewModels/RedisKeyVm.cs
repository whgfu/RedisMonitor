﻿using RedisDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Redis;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Practices.Prism.Regions;
using System.Collections.ObjectModel;

namespace RedisModule.ViewModels
{
    public class RedisKeyVm : BindableBase
    {
        private string _key;
        private string _newKey;
        private string _currentFormat;
        private bool _isSelected;
        private IRegionManager _regionManager;

        public RedisKeyVm(RedisDatabaseVm redisDatabaseVm)
        {
            _regionManager = ModuleUtils.Resolve<RegionManager>();
            Parent = redisDatabaseVm;

            ReloadCommand = new RelayCommand(Reload);
            SaveCommand = new RelayCommand(Save);
            OKCommand = new RelayCommand(OK);
            CancelCommand = new RelayCommand(Cancel);
            ExecRenameCommand = new RelayCommand(ExecRename);

            DeletekeyCommand = new RelayCommand(DeleteKey);
            RenameCommand = new RelayCommand(Rename);

            CloseTabCommand = new RelayCommand(CloseTab);

            CodeFormats = new ObservableCollection<string>() {
                "Plain Text", "JavaScript","C#"
            };

            CurrentFormat = "Plain Text";
        }


      

        public RedisKeyVm(RedisDatabaseVm redisDatabaseVm, RedisKey key):this(redisDatabaseVm)
        {
            Key = key;
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (SetProperty(ref _isSelected, value, () => IsSelected) && value)
                {
                    OnSelectedChanged();
                }
            }
        }

        public string Key
        {
            get { return _key; }
            set {

                SetProperty(ref _key, value, "Key");

            }
        }

        public string NewKey { get { return _newKey; } set { SetProperty(ref _newKey, value, () => NewKey); } }

        private object _value;

        public object Value
        {
            get { return _value; }
            set { SetProperty(ref _value, value, () => Value); }
        }

        public ObservableCollection<string> CodeFormats { get; set; }

        public string CurrentFormat
        {
            get { return _currentFormat; }
            set { SetProperty(ref _currentFormat, value, () => CurrentFormat); }
        }

        public RedisDatabaseVm Parent { get; private set; }

        public ICommand DeletekeyCommand { get; private set; }
        public ICommand RenameCommand { get; private set; }
        public ICommand ExecRenameCommand { get; private set; }

        public ICommand ReloadCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand OKCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand CloseTabCommand { get; private set; }


        public bool DialogResult { get; private set; }
        private void OnSelectedChanged()
        {
            if (SelectedChanged != null)
            {
                SelectedChanged(this, new EventArgs());
            }
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", this.Parent, Key);
        }

        public event EventHandler SelectedChanged;


        private void Reload(object obj)
        {
            this.Parent.Parent.GetValue(this.Parent.Index, Key)
                 .ContinueWith(s => Value = s.Result, TaskScheduler.FromCurrentSynchronizationContext()); 
        }

        private void DeleteKey(object obj)
        {
            this.Parent.Parent.DeleteKey(this.Parent.Index, Key)
                .ContinueWith(s =>
                {
                    if (s.Result)
                        this.Parent.RedisKeyVms.Remove(this);
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void Rename(object obj)
        {
            this.Title = "重命名主键";
            this.Parent.Parent.Parent.RaiseRenameKeyViewRequest.Raise(this, o =>
            {
                if(o.DialogResult )
                {
                    this.Key = NewKey;
                }
            });
            //this.Parent.Parent.Rename(this.Parent.Index, Key);
        }

        private void ExecRename(object obj)
        {
            this.Parent.Parent.Rename(this.Parent.Index, Key, NewKey);
            DialogResult = true;
            Close();
        }


        private void Save(object obj)
        {
            if (Value == null) return;
            RedisValue value = new RedisValue();
            value = Value.ToString();
            this.Parent.Parent.SetValue(this.Parent.Index, Key, value);
        }

        private void Cancel(object obj)
        {
            DialogResult = false;
            Close();
        }

        private void CloseTab(object obj)
        {
            var tabItem = (System.Windows.Controls.TabItem)obj;
            var view = tabItem.Content;
            var region = _regionManager.Regions["CenterRegion"];

            if (region != null)
            {
                region.Remove(view);
            }
        }
        private void OK(object obj)
        {
            Save(obj);
            DialogResult = true;
            Close();
        }
    }
}
