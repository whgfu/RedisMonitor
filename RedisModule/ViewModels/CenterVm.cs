﻿using RedisDomain;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RedisModule.ViewModels
{
    public class CenterVm: BindableBase
    {
        private string _header;
        private bool _isConnected;
        private ConnectionMultiplexer _redis;
        private ConnectManageVm _connectManageVm;

        public CenterVm(ConnectManageVm connectManageVm)
        {
            _connectManageVm = connectManageVm;

            Tabs = new ObservableCollection<TabItemInfo>();
        }

        private void connectManageVm_SelectedChanged(object sender, EventArgs e)
        {
            var redisConnectionVm = _connectManageVm.CurrentRedisConnection;

            _redis = redisConnectionVm.ConnectionMultiplexer;
            Header = redisConnectionVm.ToString();
            IsConnected = redisConnectionVm.IsConnected;
            

            if(_redis.IsConnected)
            {
                var endPoint = _redis.GetEndPoints().FirstOrDefault();
                var server = _redis.GetServer(endPoint);

                var info  = server.Info();

                Tabs.Clear();
                foreach (var item in info)
                {
                    Tabs.Add(new TabItemInfo(item));
                }
            }
        }


        public  string Header
        {
            get { return _header; }
            set
            {
                SetProperty(ref _header, value, "Header");
            }
        }
    

        public bool IsConnected
        {
            get { return _isConnected; }
            set { SetProperty(ref _isConnected,value, "IsConnected"); }
        }


        public ObservableCollection<TabItemInfo> Tabs { get; set; }

    }

    
}
