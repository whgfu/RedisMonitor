﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Redis;
using RedisModule.Properties;

namespace RedisModule.ViewModels
{
    public class TabItemInfo
    {
        public string Header { get; set; }

        public List<KeyValuePair<string, string>> KeyValueInfos { get; set; }

        public TabItemInfo(IGrouping<string, KeyValuePair<string, string>> item)
        {
            Header = Localization( item.Key);

            KeyValueInfos = Localization(item);
        }



        public TabItemInfo(ClientInfo client)
        {
            Header = client.ToString();

            KeyValueInfos = new List<KeyValuePair<string, string>>();


            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Address)), client.Address.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.AgeSeconds)), client.AgeSeconds.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.ClientType)), client.ClientType.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Database)), client.Database.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Flags)), client.Flags.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Host)), client.Host.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Id)), client.Id.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.IdleSeconds)), client.IdleSeconds.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.LastCommand)), client.LastCommand.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Name)), client.Name.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.PatternSubscriptionCount)), client.PatternSubscriptionCount.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.Port)), client.Port.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.SubscriptionCount)), client.SubscriptionCount.ToString()));
            KeyValueInfos.Add(new KeyValuePair<string, string>(Localization(nameof(client.TransactionCommandLength)), client.TransactionCommandLength.ToString()));
        }


        private string Localization(string item)
        {
            var name = Resources.ResourceManager.GetString(item);

            if(name != null)
            {
                item = name;
            }

            return item;
        }

        private List<KeyValuePair<string, string>> Localization(IGrouping<string, KeyValuePair<string, string>> items)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            foreach (var item in items)
            {
                var key = item.Key;
                var value = item.Value;
                var name = Resources.ResourceManager.GetString(item.Key);

                if(name != null)
                {
                    key = name;
                }
                result.Add(new KeyValuePair<string, string>(key, value));
            }

            return result;
        }
    }
}
