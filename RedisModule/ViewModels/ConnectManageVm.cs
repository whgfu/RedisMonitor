﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using RedisDomain;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace RedisModule.ViewModels
{
    public class ConnectManageVm : BindableBase
    {
        private IRegionManager _regionManager;
        private RedisConnectionVm _currentRedisConnection;
        public ConnectManageVm()
        {
            _regionManager = ModuleUtils.Resolve<RegionManager>();
            //_centerRegion = regionManager.Regions["CenterRegion"];
            RedisConnectionVms = new ObservableCollection<RedisConnectionVm>();

            DisconnectCommand = new RelayCommand(Disconnection, o => CurrentRedisConnection != null);
            RaiseConnectViewCommand = new RelayCommand(RaiseConnectView);
            RaiseConnectViewRequest = new InteractionRequest<RedisConnectionVm>();
            RaiseAddKeyViewRequest = new InteractionRequest<RedisKeyVm>();
            RaiseRenameKeyViewRequest = new InteractionRequest<RedisKeyVm>();
        }
        public ObservableCollection<RedisConnectionVm> RedisConnectionVms { get; private set; }


        public RedisConnectionVm CurrentRedisConnection
        {
            get { return _currentRedisConnection; }
            set { SetProperty(ref _currentRedisConnection, value, () => CurrentRedisConnection); }
        }


        public ICommand DisconnectCommand { get; private set; }

        public ICommand RaiseConnectViewCommand { get; private set; }

        public InteractionRequest<RedisConnectionVm> RaiseConnectViewRequest { get; private set; }
        public InteractionRequest<RedisKeyVm> RaiseAddKeyViewRequest { get; private set; }
        public InteractionRequest<RedisKeyVm> RaiseRenameKeyViewRequest { get; private set; }

        private void RaiseConnectView(object obj)
        {
            this.RaiseConnectViewRequest.Raise(new RedisConnectionVm { Title = "连接到Redis服务器", Content = "" }, vm =>
            {
                if(vm.DialogResult)
                {
                    if(RedisConnectionVms.FirstOrDefault(o=>o.ToString() == vm.ToString())==null)
                    {
                        vm.SelectedChanged += RedisConnectionVm_SelectedChanged;
                        RedisConnectionVms.Add(vm);
                    }
                }
            });
        }

        private void RedisConnectionVm_SelectedChanged(object sender, EventArgs e)
        {
            CurrentRedisConnection = sender as RedisConnectionVm;

            var region = _regionManager.Regions["CenterRegion"];
            var view = region.GetView("CenterConnectView"+ CurrentRedisConnection);
            if (view == null)
            {
                view = ModuleUtils.Resolve<Views.CenterConnectView>();
                region.Add(view, "CenterConnectView" + CurrentRedisConnection);
            }
            region.Activate(view);
        }

        private void Disconnection(object obj)
        {
            CurrentRedisConnection.SelectedChanged -= RedisConnectionVm_SelectedChanged;
            RedisConnectionVms.Remove(CurrentRedisConnection);
        }
    }
}
