﻿using RedisDomain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedisModule.ViewModels
{
    public class HelloVm
    {
        public HelloVm( IHelloDomain helloDomain)
        {
            Hello = "Tell me why";
        }
        public string Hello { get; set; }
    }
}
