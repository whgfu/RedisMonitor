﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using RedisDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace RedisModule.ViewModels
{
    public class InteractionRequestVm : BindableBase
    {
        private string result;
        public InteractionRequestVm()
        {
            RaiseConnectViewRequest = new InteractionRequest<Notification>();
            RaiseConnectViewCommand = new RelayCommand(RaiseConnectView);
        }

        public string Result
        {
            get
            {
                return this.result;
            }

            set
            {
                SetProperty(ref result, value, () => Result);
            }
        }


        public ICommand RaiseConnectViewCommand { get; set; }

        public InteractionRequest<Notification> RaiseConnectViewRequest { get; private set; }


        private void RaiseConnectView(object obj)
        {
            this.RaiseConnectViewRequest.Raise(
               new Notification { Content = "nihao", Title = "Custom Popup" });
        }
    }
}
