﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using RedisDomain;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace RedisModule.ViewModels
{
    public class RedisDatabaseVm : BindableBase
    {
        private IRegionManager _regionManager;
        private bool _isSelected;
        private const string Name = "Db";

        public RedisDatabaseVm(RedisConnectionVm vm,int index)
        {
            Index = index;
            Parent = vm;
            _regionManager = ModuleUtils.Resolve<RegionManager>();
            RedisKeyVms = new ObservableCollection<RedisKeyVm>();

            AddKeyCommand = new RelayCommand(AddKey);
            CloseTabCommand = new RelayCommand(CloseTab);
        }

    
        public int Index { get; set; }


        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (SetProperty(ref _isSelected, value, () => IsSelected) && value)
                {
                    OnSelectedChanged();
                }
            }
        }

        private RedisKeyVm _redisKeyVm;

        public RedisKeyVm CurrentRedisKey
        {
            get { return _redisKeyVm; }
            set { SetProperty(ref _redisKeyVm, value, () => CurrentRedisKey); }
        }

        public ICommand AddKeyCommand { get;private set; }
        public ICommand CloseTabCommand { get;private set; }

     

        public RedisConnectionVm Parent { get; private set; }

        private void OnSelectedChanged()
        {
            if (SelectedChanged != null)
            {
                SelectedChanged(this, new EventArgs());
            }

            var keys = Parent.Keys(Index);

            RedisKeyVms.Clear();
            foreach (var key in keys)
            {
                Add(key);
            }
        }

        private void RedisKey_SelectedChanged(object sender, EventArgs e)
        {
            this.Parent.CurrentRedisDatabase = this;
            CurrentRedisKey = sender as RedisKeyVm;

            var region = _regionManager.Regions["CenterRegion"];

            var view = region.GetView("CenterKeyView"+ CurrentRedisKey);
            if (view == null)
            {
                view = ModuleUtils.Resolve<Views.CenterKeyView>();
                region.Add(view, "CenterKeyView"+ CurrentRedisKey);
            }

            region.Activate(view);

        }

        public string ShortName
        {
            get
            {
                return string.Format("{0}{1}", Name, Index);
            }
        }

        private void AddKey(object obj)
        {
            var m = new RedisKeyVm(this);
            m.Title = "添加主键";
            Parent.Parent.RaiseAddKeyViewRequest.Raise(m, o =>
            {
                if(o.DialogResult)
                {
                    m.SelectedChanged += RedisKey_SelectedChanged;
                    RedisKeyVms.Add(m);
                }
            });
        }

        private void Add(RedisKey key)
        {
            var model = new RedisKeyVm(this, key);
            model.SelectedChanged += RedisKey_SelectedChanged;
            RedisKeyVms.Add(model);
        }


        private void CloseTab(object obj)
        {
            var tabItem = (System.Windows.Controls.TabItem)obj;
            var view = tabItem.Content;
            var region = _regionManager.Regions["CenterRegion"];

            if (region != null)
            {
                region.Remove(view);
            }
        }

        public override string ToString()
        {
            return string.Format("{1}:{0}", ShortName, this.Parent);
        }

        public ObservableCollection<RedisKeyVm> RedisKeyVms { get; set; }

        public event EventHandler SelectedChanged;
    }
}
