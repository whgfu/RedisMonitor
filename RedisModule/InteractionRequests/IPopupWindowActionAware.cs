﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace RedisModule.InteractionRequests
{
    public interface IPopupWindowActionAware
    {
        Window HostWindow { get; set; }

        Notification HostNotification { get; set; }
    }
}
