﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RedisModule.InteractionRequests.DefaultWindows
{
    /// <summary>
    /// DefaultConfirmationWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DefaultConfirmationWindow 
    {
        /// <summary>
        /// The content template to use when showing <see cref="Confirmation"/> data.
        /// </summary>
        public static readonly DependencyProperty ConfirmationTemplateProperty =
            DependencyProperty.Register(
                "ConfirmationTemplate",
                typeof(DataTemplate),
                typeof(DefaultConfirmationWindow),
                new PropertyMetadata(null));

        public DefaultConfirmationWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The content template to use when showing <see cref="Confirmation"/> data.
        /// </summary>
        public DataTemplate ConfirmationTemplate
        {
            get { return (DataTemplate)GetValue(ConfirmationTemplateProperty); }
            set { SetValue(ConfirmationTemplateProperty, value); }
        }

        public Microsoft.Practices.Prism.Interactivity.InteractionRequest.Confirmation Confirmation
        {
            get
            {
                return this.DataContext as Microsoft.Practices.Prism.Interactivity.InteractionRequest.Confirmation;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Confirmation.Confirmed = true;
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
