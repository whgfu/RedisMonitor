﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RedisModule.InteractionRequests.DefaultWindows
{
    /// <summary>
    /// DefaultNotificationWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DefaultNotificationWindow 
    {
        public static readonly DependencyProperty NotificationTemplateProperty =
          DependencyProperty.Register(
              "NotificationTemplate",
              typeof(DataTemplate),
              typeof(DefaultNotificationWindow),
              new PropertyMetadata(null));

        public DefaultNotificationWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// The <see cref="DataTemplate"/> to apply when displaying <see cref="Notification"/> data.
        /// </summary>
        public DataTemplate NotificationTemplate
        {
            get { return (DataTemplate)GetValue(NotificationTemplateProperty); }
            set { SetValue(NotificationTemplateProperty, value); }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
