﻿using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.ViewModel;
using RedisModule.InteractionRequests;
using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows;

namespace RedisDomain
{
    public class BindableBase :Notification, INotifyPropertyChanged, IPopupWindowActionAware
    {
        public Window HostWindow { get; set; }

        public Notification HostNotification { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T storage, T value,  string propertyName = null)
        {
            if (object.Equals(storage, value)) return false;

            storage = value;
            this.OnPropertyChanged(propertyName);

            return true;
        }


        protected virtual bool SetProperty<T>(ref T storage,T value,Expression<Func<T>> propertyExpression)
        {
            if (object.Equals(storage, value)) return false;

            storage = value;
            this.OnPropertyChanged(propertyExpression);

            return true;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var propertyName = PropertySupport.ExtractPropertyName(propertyExpression);
            this.OnPropertyChanged(propertyName);
        }


        protected void Close()
        {
            if (this.HostWindow != null)
            {
                this.HostWindow.Close();
            }
        }
    }
}
