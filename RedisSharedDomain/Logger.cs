﻿using Microsoft.Practices.Prism.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedisDomain
{
    public class Logger : ILoggerFacade
    {
        public void Log(string message, Category category, Priority priority)
        {
            if (LogCallback != null)
                LogCallback(message, category, priority);
        }

        public static ILoggerFacade _instance;
        public static ILoggerFacade Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Logger();
                }
                return _instance;
            }
        }

        public static Action<string, Category, Priority> LogCallback { get; internal set; }

        public static void Log(string message)
        {
            Instance.Log(message, Category.Debug, Priority.Low);
        }
    }
}
