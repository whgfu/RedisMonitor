﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Logging;
using RedisModule.ViewModels;

namespace RedisDomain
{
    public static class ModuleUtils
    {
        private static Func<Type, object>  _resolve ;
        private static Action<Type, Type> _registerType;
        private static Action<Type, object> _registerInstance;


        private static void RegisterType<TFrom,TTo>()
        {
            var fromType = typeof(TFrom);
            var toType = typeof(TTo);

            _registerType(fromType, toType);
        }


        public static T Resolve<T> ()
        {
            var type = typeof(T);
            if(_resolve != null)
            {
                return (T)_resolve(type);
            }
            return default(T);
        }


        public static void Init(Func<Type, object> resolve, Action<Type, Type> registerType, Action<Type> registerSingleType, Action<Type, object> registerInstance)
        {
            _resolve = resolve;
            _registerType = registerType;
            _registerInstance = registerInstance;

            RegisterType<Services.IHelloDomain, Services.HelloDomain>();
            RegisterType<Services.IWorldDomain, Services.WorldDomain>();

            registerInstance(typeof(ConnectManageVm), new ConnectManageVm());
        }
    }
}
