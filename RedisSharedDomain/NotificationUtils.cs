﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedisDomain
{
    public class NotificationUtils
    {
        public static Action<string> ShowAction
        {
            get; set;
        }

        public static void Show(String format, params object[] args)
        {
            ShowAction(string.Format(format, args));
        }
    }
}
