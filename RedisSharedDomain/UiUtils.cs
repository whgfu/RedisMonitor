﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace RedisDomain
{
    public static class UiUtils
    {
        public static T FindParent<T>(DependencyObject current)
            where T : DependencyObject
        {
            var parentObject = VisualTreeHelper.GetParent(current);
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }


        public static T FindChild<T>(DependencyObject depObj)
             where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? FindChild<T>(child);
                if (result != null) return result;
            }
            return null;
        }
    }
}
